//
//  FetchyFeedCell.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

class FetchyFeedCell: UICollectionViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var photo: UIImageView!
    private var imageUrlString: String?
    
    private var downloadTask: URLSessionDownloadTask?
    public var imageURL: URL? {
        didSet{
            
            self.downloadItemImageForSearchResult(imageURL: imageURL)
        }
    }

    public var shopTitle: String?{
        
        didSet{
            self.title.text = shopTitle
        }
    }

    public func downloadItemImageForSearchResult(imageURL: URL?) {
        
        if let urlOfImage = imageURL {
            if let cachedImage = imageCache.object(forKey:     urlOfImage.absoluteString as NSString){
                self.photo!.image = cachedImage as? UIImage
            }
            else {
                let session = URLSession.shared
                self.downloadTask = session.downloadTask(
                    with: urlOfImage as URL, completionHandler: { [weak self] url, response, error in
                        if error == nil, let url = url, let data = NSData(contentsOf: url), let image = UIImage(data: data as Data) {
                            
                            DispatchQueue.main.async() {
                                
                                let imageToCache = image
                                
                                if let strongSelf = self, let imageView = strongSelf.photo {
                                    
                                    imageView.image = imageToCache
                                    
                                    imageCache.setObject(imageToCache, forKey: urlOfImage.absoluteString as NSString , cost: 4)
                                }
                            }
                        }
                        else {
                            //will eventually cancel because of server limit for concurrent requests 
                            //print("Error: \(String(describing: error?.localizedDescription))")
                        }
                })
                self.downloadTask!.resume()
            }
        }
    }
    
    override public func prepareForReuse() {
        self.downloadTask?.cancel()
        photo?.image = UIImage(named: "appleStore")
    }
    
    deinit{
        self.downloadTask?.cancel()
        photo?.image = nil
    }

}

