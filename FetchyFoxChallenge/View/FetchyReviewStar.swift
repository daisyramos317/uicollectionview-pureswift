//
//  FetchyReviewStar.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/21/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

enum FetchyStarType {
    case empty, halfEmpty, full
}

class FetchyReviewStar: UIView {

    var type: FetchyStarType = .empty {
        didSet {
            switch type {
            case .empty:
                starView.image = UIImage(named: "StarEmpty")?.tint(with: UIColor.cyan)
            case .halfEmpty:
                starView.image = UIImage(named: "StarHalfEmpty")?.tint(with: UIColor.cyan)
            case .full:
                starView.image = UIImage(named: "StarFilled")?.tint(with: UIColor.cyan)
            }
        }
    }
    
    var starView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(starView)
        
        starView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        starView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        starView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        starView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}
