//
//  FetchyFoxFeedView.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit



class FetchyFoxFeedView: UICollectionViewController {
    
    let reuseIdentifier = "fetchyCell"
    public var items: [Shop]? = []
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FetchFinishedNotification), object: self)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FetchFailedNotification), object: self)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(FetchyFoxFeedView.fetchFinished(notification:)), name: NSNotification.Name(rawValue: FetchFinishedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FetchyFoxFeedView.fetchFailed(notification:)), name: NSNotification.Name(rawValue: FetchFailedNotification), object: nil)
       
    }
    
    
    func fetchFinished(notification: Notification){
        
        if(self.items != nil){
            if(self.items!.count < 1){
                self.items = FetchyFeedDataManager.shared.getShops()
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
            }
        }
        
    }
    
    func fetchFailed(notification: Notification){
         self.displayAlertMessage(message: "Fetch failed please check your internet connection and try again")
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        if (self.items != nil){
            print(items!.count)
            return (items?.count)!
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! FetchyFeedCell
        let shop = items![indexPath.row]
        cell.imageURL = URL(string: shop.photoUrl)
        cell.shopTitle = shop.title
        
        return cell
    }
    
     // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {
        
        if let cell = collectionView.cellForItem(at: indexPath) as? FetchyFeedCell {
            self.performSegue(withIdentifier: "showDetail", sender: cell)
        }

    }

    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         assert(sender as? FetchyFeedCell != nil, "sender is not a collection view")
        
        if let indexPath = self.collectionView?.indexPath(for: sender as! FetchyFeedCell) {
            if segue.identifier == "showDetail" {
                let detailVC: FetchyShopDetailController = segue.destination as! FetchyShopDetailController
                let shop = items?[indexPath.row]
                detailVC.shop = shop
            }
        } else {
            // Error sender is not a cell or cell is not in collectionView.
        }
     
    }
 

}

