//
//  FetchyShopDetailController.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/21/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

class FetchyShopDetailController: UIViewController {

    public var shop: Shop?{
        didSet{
            if let shopUrl = shop?.photoUrl {
                imageView.imageFromURL(urlString: shopUrl)
            }
            
            if let title = shop?.title {
                titleLabel.text = title
            }
            
            if let hours = shop?.hours {
                hoursLabel.text = hours
            }
            
            if let aboutShop = shop?.shopDescription {
                aboutShopTextView.text = aboutShop
            }
            
             let reviewCount = String(describing: shop!.numOfReviews!)
                reviewCountLabel.text = reviewCount
            
            
             let price = self.convertPriceIntToText(priceRange: (shop?.priceRange!)!)
                priceLabel.text = price
            
            
            let review = Double((shop?.rating)!) 
                reviewView.rating = review
            
            if shop?.popular == 1 {
                categoryLabel.text = "Popular"
            } else if shop?.new == 1{
                categoryLabel.text = "New"
            } else {
                categoryLabel.isHidden = true
            }

            
            
        }
    }
   
    let sideMargin: CGFloat = 40
    
    var scrollView: UIScrollView = {
        let view = UIScrollView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.alwaysBounceVertical = true
        return view
    }()
    
    var footerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: -1)
        view.layer.shadowOpacity = 0.1
        
        return view
    }()
    
    lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "appleStore")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(FetchyShopDetailController.handleBackPressed), for: .touchUpInside)
        
        let img = UIImage(named: "Back")
        button.setImage(img, for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        return button
    }()
    
    var titleLabel: UILabel = {
        let view = UILabel()
        view.adjustsFontSizeToFitWidth = true 
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.boldSystemFont(ofSize: 30)
        view.textColor = UIColor.darkGray
        return view
    }()
    
    var hoursLabel: VerticalAlignLabel = {
        let view = VerticalAlignLabel()
        view.adjustsFontSizeToFitWidth = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.boldSystemFont(ofSize: 18)
        view.verticalAlignment = .bottom
        return view
    }()
    
    var categoryLabel: VerticalAlignLabel = {
        let view = VerticalAlignLabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 16)
        view.textColor = UIColor.lightGray
        return view
    }()
    
    
    var separator: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    

    
    var aboutShopTextView: UITextView = {
        let view = UITextView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = UIColor.darkGray
        view.font = UIFont.systemFont(ofSize: 16)
        return view
    }()
    
    var priceLabel: VerticalAlignLabel = {
        let view = VerticalAlignLabel()
        view.adjustsFontSizeToFitWidth = true 
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.boldSystemFont(ofSize: 14)
        view.textColor = UIColor.black
        view.verticalAlignment = .bottom
        view.textAlignment = .left
        return view
    }()
    
    var priceCategoryLabel: VerticalAlignLabel = {
        let view = VerticalAlignLabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.systemFont(ofSize: 14)
        view.textColor = UIColor.black
        view.text = "Price Range"
        view.verticalAlignment = .bottom
        view.textAlignment = .left
        return view
    }()
    
    var reviewView: FetchyFoxReview = {
        let view = FetchyFoxReview()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rating = 3.5
        return view
    }()
    
    var reviewCountLabel: VerticalAlignLabel = {
        let view = VerticalAlignLabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = UIFont.boldSystemFont(ofSize: 11)
        view.textColor = UIColor.darkGray
        view.textAlignment = .left
        view.verticalAlignment = .middle
        return view
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    

    func setupViews() {
        
        view.addSubview(scrollView)
        
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -80).isActive = true
        
        view.addSubview(footerView)
        
        footerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        footerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        footerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        footerView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        
        setupScrollContent()
        setupFooterViews()
    }
    
    func setupScrollContent() {
        scrollView.addSubview(imageView)
        
        imageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        scrollView.addSubview(backButton)
        
        backButton.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 10).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        backButton.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 10).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        scrollView.addSubview(titleLabel)
        
        titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -sideMargin).isActive = true
        
        setupShopViews()
        
    }
    
    func setupShopViews() {
     
        
        scrollView .addSubview(hoursLabel)
        
        hoursLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
        hoursLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        hoursLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: sideMargin / 2).isActive = true
    
        
        scrollView .addSubview(categoryLabel)
        
        categoryLabel.topAnchor.constraint(equalTo: hoursLabel.bottomAnchor).isActive = true
        categoryLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        categoryLabel.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: sideMargin / 2).isActive = true
        categoryLabel.widthAnchor.constraint(equalToConstant: 75).isActive = true
        
        
        
        scrollView.addSubview(separator)
        
       
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        separator.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -sideMargin).isActive = true
        
        scrollView.addSubview(aboutShopTextView)
        
        aboutShopTextView.topAnchor.constraint(equalTo: separator.bottomAnchor, constant: 450).isActive = true
        aboutShopTextView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        aboutShopTextView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        aboutShopTextView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -sideMargin).isActive = true
        
        
    }
    
    func setupFooterViews() {
        
        
        footerView.addSubview(priceLabel)
        
        priceLabel.leftAnchor.constraint(equalTo: footerView.leftAnchor, constant: sideMargin / 2).isActive = true
        priceLabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        priceLabel.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 15).isActive = true
        priceLabel.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 1/2, constant: -15).isActive = true
        
        footerView.addSubview(priceCategoryLabel)
        
        priceCategoryLabel.leftAnchor.constraint(equalTo: priceLabel.rightAnchor, constant: 5).isActive = true
        priceCategoryLabel.topAnchor.constraint(equalTo: footerView.topAnchor, constant: 15).isActive = true
        priceCategoryLabel.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 1/2, constant: -15).isActive = true
        
        footerView.addSubview(reviewView)
        
        reviewView.leftAnchor.constraint(equalTo: footerView.leftAnchor, constant: sideMargin / 2).isActive = true
        reviewView.widthAnchor.constraint(equalToConstant: reviewView.starSize * CGFloat(reviewView.stars.count)).isActive = true
        reviewView.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -15).isActive = true
        reviewView.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 1/2, constant: -15).isActive = true
        
        footerView.addSubview(reviewCountLabel)
        
        reviewCountLabel.leftAnchor.constraint(equalTo: reviewView.rightAnchor, constant: 5).isActive = true
        reviewCountLabel.bottomAnchor.constraint(equalTo: footerView.bottomAnchor, constant: -15).isActive = true
        reviewCountLabel.heightAnchor.constraint(equalTo: footerView.heightAnchor, multiplier: 1/2, constant: -15).isActive = true
        
    }
    
    func handleBackPressed() {
        dismiss(animated: true, completion: nil)
    }
}
