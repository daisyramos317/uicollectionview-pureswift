//
//  UIAlertView+UIViewController.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit


extension UIViewController {
    func displayAlertMessage(message: String)
    {
        
        let myAlert = UIAlertController(title:"Alert", message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler:nil)
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
    }
    
}
