//
//  PriceRange+UIViewController.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/21/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import UIKit

extension UIViewController {
func convertPriceIntToText(priceRange: Int) -> String {
    
    if priceRange == 1 {
        return "$"
    }
    else if priceRange == 2 {
        return "$$"
        
    } else if priceRange == 3 {
        return "$$$"
        
    } else if priceRange == 4 {
        return "$$$$"
        
    } else if priceRange == 5 {
        return "$$$$$"
        
    }
    
    return ""
    
}
}
