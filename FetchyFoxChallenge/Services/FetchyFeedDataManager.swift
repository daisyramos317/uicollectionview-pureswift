
//
//  FetchyFeedDataManager.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation


class FetchyFeedDataManager: NSObject {

    fileprivate var shops: [Shop]!
    static let shared = FetchyFeedDataManager()
    
    private override init(){
        self.shops = [Shop]()
        super.init()
    }
    

    public func addShopFromDictionary(dictionary: Dictionary<String, Any>){
        
        let shop: Shop = Shop(dictionary: dictionary)
        self.shops.append(shop)
        
    }
    
    public func getShops() -> [Shop]{
        var shops: [Shop] = [Shop]()
        for shop: Shop in self.shops{
            shops.append(shop)
        }
        
        return shops
    }
    

    
}
