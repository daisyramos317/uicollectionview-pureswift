//
//  FetchService.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation

let FetchFinishedNotification: String = "FetchFinishedNotification"
let FetchFailedNotification: String = "FetchFailedNotification"


final class FetchService {
    
    static let shared = FetchService()
    fileprivate var shops: [Shop] = []
    
    fileprivate var baseUrl: String = "https://fetchy-interview.herokuapp.com/api/placedata"
    lazy var session: URLSession = URLSession.shared
    
   public func fetchShops(completionHandler : ((_ shops: [Dictionary<String, Any>]?, _ error: Error?) -> Void)?) {
      
        guard let url = URL(string: baseUrl) else {
            fatalError()
        }
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "GET"
    
        
        let task = session.dataTask(with: request as URLRequest) {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response, error == nil else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: FetchFailedNotification), object: nil)
                return
            }
         
            do {
                if let responseObject = try JSONSerialization.jsonObject(with: data!) as? [String: Any],
                    let data = responseObject["data"] as? [Dictionary<String, Any>]{

                    
                    completionHandler?(data, nil)
                  
                }
                
                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: FetchFinishedNotification), object: nil)
                    
                    
            
            } catch let error as NSError {
                completionHandler?(nil, error)
                print(error)
                
            }
        }
        
        task.resume()
    }
}
