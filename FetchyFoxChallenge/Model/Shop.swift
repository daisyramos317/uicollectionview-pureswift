//
//  Shop.swift
//  FetchyFoxChallenge
//
//  Created by Daisy Ramos on 6/19/17.
//  Copyright © 2017 Daisy Ramos. All rights reserved.
//

import Foundation


class Shop: NSObject {
    let id: String!
    let hours: String!
    let photoUrl: String!
    let shopDescription: String!
    let title: String!
    let popular: Int!
    let new: Int!
    let rating: Int!
    let numOfReviews: Int!
    let priceRange: Int!
    
    init(dictionary: [String: Any]){
        
        self.id = dictionary["id"] as? String
        self.hours = dictionary["hours"] as? String
        self.photoUrl = dictionary["url"] as? String
        self.shopDescription = dictionary["description"] as? String
        self.title = dictionary["title"] as? String
        self.popular = dictionary["popular"] as? Int
        self.new = dictionary["new"] as? Int
        self.rating = dictionary["rating"] as? Int
        self.numOfReviews = dictionary["n_reviews"] as? Int
        self.priceRange =  dictionary["price_range"] as? Int
        
    }
    

}

